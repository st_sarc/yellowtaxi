import json
import datetime as dt
from yellowtaxi.models import Taxi, TraceLink, Journey, ClientTable
from django.db import connections
import time as tt
from django.db import connections
from django.db.models import Q
from getResponse import getResponse
from shortcodegen import generate

def bookAndLink(data, method):
    print "data"
    print "data = " +str(data)
    # if method == "GET":
    #     regno = data["regno"]
    #     starttime = data["start"]
    #     endtime = data["end"]
    #     uniqueID = generate()
    #     op = connections['default'].cursor()
    #     b = TraceLink(taxi_id = Taxi.objects.get(taxi_number = regno), starttime = starttime, endtime = endtime, unique_id = uniqueID)
    #     b.save()
    #     return getResponse("OK", "All set", uniqueID)

    client_phone = data["phone_no"]
    passengers_num = data["passengers_num"]
    carpool_status = data["carpool_status"]
    taxi_number = data["taxi_number"]
    from_address = data["from_address"]
    to_address = data["to_address"]
    start_time = dt.datetime.now()
    try:
        b = Journey(taxi_id = Taxi.objects.get(taxi_number = taxi_number).id, client_id = ClientTable.objects.get(phone_no = client_phone).id, start_time = start_time, from_address = from_address, to_address = to_address, unique_id = generate(), journey_status = 1)
        b.save()
        if data["carpool_status"] == "Enable Carpool":
            carpool_status = 1
        if data["carpool_status"] == "Disable Carpool":
            carpool_status = 2

        k = Taxi.objects.get(taxi_number = taxi_number)
        m = k
        m.status = carpool_status
        m.current_passengers_num = Taxi.objects.get(taxi_number = taxi_number).current_passengers_num + int(passengers_num)
        m.save()
        if method == "POST" : 
            return getResponse("Ok", "All set", {"journey_id" : b.unique_id, "journey_link" : "/yellowtaxi/showtrace/?code="+b.unique_id+"&submit="})
        if method == "GET" :
            return getResponse("OK", "All set", b.unique_id)
    except Exception as e:
        return getResponse("Not Ok", "Check the response", str(e))