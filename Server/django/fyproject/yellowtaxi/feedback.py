import json
import datetime as dt
from yellowtaxi.models import Feedback, Taxi
from django.db import connections
import time as tt
from django.db import connections
from django.db.models import Q
from getResponse import getResponse

def update(data, method):
    #form = cgi.FieldStorage() 
    try:
        name = data["name"]
        emailid = data["emailid"]
        regno = data["regno"]
        feedback = data["feedback"]
        rating = data["rating"]
    except:
        return getResponse("Not Ok", "How fucking hard is to send the right parameters?!", "Look at the audacity. Expecting a response!")
    op = connections['default'].cursor()
    try:
        b = Feedback(name = name, emailid = emailid, taxi_id = Taxi.objects.get(taxi_number = regno).id, feedback = feedback, rating = rating)
        b.save()

        qs1 = (Feedback.objects.filter(taxi_id = Taxi.objects.filter(taxi_number = regno)[0].id))

        qs2 = Taxi.objects.filter(taxi_number = regno)
        current_rating = qs2[0].rating
        rating = float(rating)
        x = qs2[0]
        x.rating = (current_rating+rating)/len(qs1)
        x.save()

        if method == "GET":
            return getResponse("1", "All set", "")
        if method == "POST":
            return getResponse("Ok", "All set", "")
    except Exception as e:
        if method == "GET":
            return getResponse("2", "All set", str(e))
        if method == "POST":
            return getResponse("Not Ok", "Couldn't save into this sorry ass database", str(e))