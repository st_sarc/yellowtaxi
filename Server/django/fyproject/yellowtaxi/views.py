from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
import json
from feedback import update
from trace import getTrace
from book import bookAndLink
from django.views.decorators.csrf import csrf_exempt
from getResponse import getResponse
from models import *
from datetime import datetime

@csrf_exempt
def home(request):
    context = {}
    #return HttpResponse(json.dumps(response, cls = EpochEncoder), "application/json")
    return render(request, 'yellowtaxi/home.html', context)
@csrf_exempt
def trace(request):
    context = {}
    return render(request, 'yellowtaxi/trace.html', context)

@csrf_exempt
def showtrace(request):
    #print "DATAsfdas = " +request.GET["code"]
    code = request.GET["code"]
    response = getTrace(code)
    context = {"response" : response}
    return render(request, 'yellowtaxi/showtrace.html', context)
@csrf_exempt
def feedback(request):
    context = {}
    return render(request, 'yellowtaxi/feedback.html', context)

@csrf_exempt
def showtaxis(request):
    context = {}
    return render(request, 'yellowtaxi/showtaxis.html', context)

@csrf_exempt
def feedbacksubmit(request):
    if request.method == "GET":
        data = request.GET
        api_response = update(data, request.method)
        #return HttpResponse(response, "text")
        print api_response
        if(api_response["status"] == "1"):
            context = {"status" : "OK"}
        else:
            try:
                context = {"status" : "Not OK", "regno" : data["regno"]}
            except:
                context = {"status" : "Not OK", "regno" : "NA"}
        return render(request, 'yellowtaxi/feedbacksubmit.html', context)
    elif request.method == "POST":
        data = request.POST
        api_response = update(data, request.method)
        return HttpResponse(json.dumps(api_response), "application/json")

@csrf_exempt
def aboutus(request):
    context = {}
    return render(request, 'yellowtaxi/aboutus.html', context)

@csrf_exempt
def book(request):
    context = {}
    return render(request, 'yellowtaxi/book.html', context)

@csrf_exempt
def booksubmit(request):
    if request.method == "GET":
        data = request.GET
        api_response = bookAndLink(data, request.method)
        context = {"code" : api_response["response"]}
        return render(request, 'yellowtaxi/booksubmit.html', context)
    elif request.method == "POST":    
        data = request.POST
        api_response = bookAndLink(data, request.method)
        print api_response
        return HttpResponse(json.dumps(api_response), "application/json")

#========================ANDROID views===================

@csrf_exempt
def clientsignup(request):
    api_response = {}
    if request.method == "POST":
        data = request.POST
        qs = ClientTable.objects.filter(phone_no = data["phone_no"])
        if qs.exists():
            api_response = getResponse("Not Ok", "Phone number already exists", "")
        else:
            try:
                new_client = ClientTable(client_name = data["name"], email_id = data["email_id"], phone_no = data["phone_no"], password = data["password"], address = data["address"])
                new_client.save()
                api_response = getResponse("Ok", "All set", "")
            except Exception as e:
                api_response = getResponse("Not Ok", "Invalid parameters", str(e))
    else:
        api_response = getResponse("Not Ok", "This is not a post request!", request)
    print api_response
    return HttpResponse(json.dumps(api_response), "application/json")

@csrf_exempt
def searchataxi(request):
    api_response = {}
    if request.method == "POST":
        try:
            data = request.POST
            latitude = float(data["from_address"].split(',')[0])
            longitude = float(data["from_address"].split(',')[1])
            if data["carpool_status"] == "Enable Carpool":
                carpool_status = 1
            if data["carpool_status"] == "Disable Carpool":
                carpool_status = 0
            
            qs = ClientTable.objects.filter(phone_no = data["phone_no"], password = data["password"])
            if qs.exists():
                try:
                    qs = Taxi.objects.filter(latitude__lte = latitude+1, latitude__gte = latitude-1, longitude__lte = longitude+1, longitude__gte = longitude-1)
                    
                except Exception as e:
                    api_response = getResponse("Not Ok", "Error in searching for taxis", "")
                    return HttpResponse(json.dumps(api_response), "application/json")

                taxis = []
                flag = 0
                if qs.exists():

                    for row in qs:
                        if (row.total_capacity - row.current_passengers_num) >= int(data["passengers_num"]):
                            if carpool_status == 0:
                                if row.status == 0:
                                    flag = 1
                                    taxis.append({"taxi_number": row.taxi_number, "driver_name" : row.driver_name, "vendor_name" : row.vendor_name, "phone_number" : row.phone_no, "rating" : row.rating})
                            if carpool_status == 1:
                                if row.status == 1 or row.status == 0:
                                    flag = 1
                                    taxis.append({"taxi_number": row.taxi_number, "driver_name" : row.driver_name, "vendor_name" : row.vendor_name, "phone_number" : row.phone_no, "rating" : row.rating})
                    if flag == 0:
                        taxis.append("No taxis found")
                else:
                    taxis.append("No taxis found")
                api_response = getResponse("Ok", "For once, you did something right", taxis)
            else:
                api_response = getResponse("Not Ok", "Invalid phone number or password", "")
        except Exception as e:
            api_response = getResponse("Not Ok", "Exception handling", str(e))
    else:
        api_response = getResponse("Not Ok", "You are supposed to send a POST request", "data")
    print api_response
    return HttpResponse(json.dumps(api_response), "application/json")

@csrf_exempt
def locationupdate(request):
    try:
        data = request.POST
        latitude = float(data["location"].split(',')[0])
        longitude = float(data["location"].split(',')[1])
        gps_timestamp = datetime.now()
        taxi_number = data["taxi_number"]
        b = LocationData(taxi_id=Taxi.objects.get(taxi_number = taxi_number).id, latitude = latitude, longitude=longitude, gps_timestamp = gps_timestamp)
        b.save()
        api_response = getResponse("Ok", "All set", "")
    except Exception as e:
        api_response = getResponse("Not Ok", "Couldn't save the location", str(e))
    print api_response
    return HttpResponse(json.dumps(api_response), "application/json")

@csrf_exempt
def driverlogin(request):
    try:
        data = request.POST
        qs = Taxi.objects.filter(taxi_number = data["taxi_number"], password = data["password"])
        api_response = getResponse("Ok", "All set", "")
    except Exception as e:
        api_response = getResponse("Not Ok", "Invalid credentials", str(e))
    print api_response
    return HttpResponse(json.dumps(api_response), "application/json")

@csrf_exempt
def getalltaxis(request):
    response =[]
    try:
        some = Taxi.objects.all()
        for row in some:
            response.append({"regno":row.taxi_number, "detail":row.vendor_name+"--"+row.taxi_number+"--"+row.driver_name})
        api_response = getResponse("Ok", "All set", response)
    except Exception as e:
        api_response = getResponse("Not Ok", "What da macha?", str(e))
    print api_response
    return HttpResponse(json.dumps(api_response), "application/json")

@csrf_exempt
def changetaxistatus(request):
    try:
        data=request.POST
        if data['status'] == "available":
            st = 0
        if data['status'] == "onboard":
            st = 1
        if data['status'] == "busy":
            st = 2
        qs = Taxi.objects.filter(taxi_number = data["taxi_number"]).update(status = st)
        api_response = getResponse("Ok", "All set", "")
    except Exception as e:
        api_response = getResponse("Not Ok", "wrong parameters probably", str(e))
    return HttpResponse(json.dumps(api_response), "application/json")