from django.db import models
import base64
import random
import hashlib

# class EmployeeInfo(models.Model):
#     reg_no = models.CharField(max_length = 100, primary_key=True)
#     name = models.CharField(max_length=100, null=True, blank=True)
#     phone_no = models.CharField(max_length=50, blank = True, null = True)
class ClientTable(models.Model):
    client_name = models.CharField(max_length = 100)
    email_id = models.CharField(max_length = 100, null=True, blank=True)
    phone_no = models.CharField(max_length = 100, unique = True)
    password = models.CharField(max_length = 100)
    address = models.CharField(max_length = 200)

class Taxi(models.Model):
    taxi_number = models.CharField(max_length = 100)
    password = models.CharField(max_length = 100)
    driver_name = models.CharField(max_length = 100)
    phone_no = models.CharField(max_length = 100)
    latitude = models.FloatField()
    longitude = models.FloatField()
    gps_timestamp = models.DateTimeField()
    current_passengers_num = models.IntegerField()
    status = models.IntegerField(default = 0)
    total_capacity = models.IntegerField()
    vendor_name = models.CharField(max_length = 100)
    vehicle_data = models.CharField(max_length = 100)
    rating = models.FloatField(null = True, blank = True)

class LocationData(models.Model):
    taxi = models.ForeignKey(Taxi)
    #reg_no = models.CharField(max_length=50, blank = True, null = True)
    #lat = models.CharField(max_length = 50)
    #lng = models.charField(max_length = 50)
    latitude = models.FloatField()
    longitude = models.FloatField()
    gps_timestamp = models.DateTimeField(blank = True, null = True)

class Feedback(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    emailid = models.CharField(max_length=100, null=True, blank=True)
    #regno = models.CharField(max_length=50, blank=True, null=True)
    taxi = models.ForeignKey(Taxi)
    feedback = models.CharField(max_length=1024, null=True, blank=True, default="")
    rating = models.IntegerField(blank=True, null=True)
    
class TraceLink(models.Model):
    taxi = models.ForeignKey(Taxi)
    starttime = models.DateTimeField(blank = True, null = True)
    endtime = models.DateTimeField(blank = True, null = True)
    unique_id = models.CharField(max_length = 100, primary_key = True)


class Journey(models.Model):
    taxi = models.ForeignKey(Taxi)
    client = models.ForeignKey(ClientTable)
    start_time = models.DateTimeField(blank = True, null = True)
    end_time = models.DateTimeField(blank = True, null = True)
    from_address = models.CharField(max_length = 300)
    to_address = models.CharField(max_length = 300)
    journey_status = models.IntegerField()
    unique_id = models.CharField(max_length = 100, unique = True)
# class ApiAuth(models.Model):
#     apikey = models.CharField(max_length=50, primary_key = True)
#     vendor = models.CharField(max_length=50)
#     op_id = models.IntegerField()
#     active = models.IntegerField()

#     def __unicode__(self):
#         return self.apikey + " - " + str(self.op_id) + " - " + self.vendor

#     @staticmethod
#     def generate():
#         apikey = base64.b64encode(hashlib.sha256( str(random.getrandbits(256)) ).digest(), random.choice(['rA','aZ','gQ','hH','hG','aR','DD'])).rstrip('==')
#         return apikey.upper()


# class JourneyLink(models.Model):
#     shortcode = models.CharField(max_length=50, primary_key=True)
#     apiauthkey = models.ForeignKey(ApiAuth)
#     starttime = models.DateTimeField()

#     class Meta:
#         index_together = [ ["apiauthkey", "uniqueID"], ]


#=========================ANDROID MODELS=============================================













#CREATE TABLE a SELECT * FROM b WHERE 0=1
#CREATE TABLE yellowtaxi_locationdata SELECT bus_id as reg_no, latitude as lat, longitude as lng, gps_timestamp FROM deviceloc where bus_id < 20
#INSERT INTO yellowtaxi_locationdata (taxi, latitude, longitude, gps_timestamp) SELECT bus_id, latitude, longitude, gps_timestamp FROM deviceloc WHERE bus_id < 20