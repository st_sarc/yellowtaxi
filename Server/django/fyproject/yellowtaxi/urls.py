from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
from yellowtaxi import views



urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='home')),
    url(r'^home/$', views.home),
    url(r'^feedback/$', views.feedback),
    url(r'^feedbacksubmit/',views.feedbacksubmit),
    url(r'^aboutus/$', views.aboutus),
    url(r'^trace/$', views.trace),
    #url(r'^showtrace/(?P<code>[A-Z0-9]{6})/$',views.showtrace),
    url(r'^showtrace/', views.showtrace),
    url(r'^book/', views.book),
    url(r'^booksubmit/',views.booksubmit),

    url(r'^showtaxis/', views.showtaxis),
    #url(r'^(?P<apikey>[A-Z0-9])/getcode/$', views.getcode),
    #url(r'^check/$', views.check),

    url(r'^getalltaxis/$', views.getalltaxis),

    #=========android urls====================
    url(r'^clientsignup/',views.clientsignup),
    url(r'^searchataxi/',views.searchataxi),
    url(r'^driverlogin/', views.driverlogin),
    url(r'^locationupdate/', views.locationupdate),
    url(r'^changetaxistatus/', views.changetaxistatus),

)