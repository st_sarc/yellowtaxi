import base64
import hashlib
import random
import os
import string

def generate():
    #apikey = base64.b64encode(hashlib.sha256( str(random.getrandbits(256)) ).digest(), random.choice(['rA','aZ','gQ','hH','hG','aR','DD'])).rstrip('==')
    shortcode = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
    return shortcode