package com.example.scratchtwo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Feedback extends Activity {

	EditText clientName;
	EditText clientEmail;
	EditText feedbackTaxiNumber;
	EditText feedbackCommentText;
	TextView successMessage;
	RadioGroup feedbackStarRadioGroup;
	RadioButton tempRadioButton;
	Button feedbackSubmitButton;

	String ServerURLSt;
	String name;
	String email;
	String regno;
	String feedback;
	String rating;
	String namePattern;
	String emailPattern;
	String ReceivedResponse;
	
	int selectedId;
	boolean flagSuccess;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);
		Intent myIntent = getIntent();
		ServerURLSt = myIntent.getStringExtra("Server URL");

		clientName = (EditText) findViewById(R.id.clientNameET);
		clientEmail = (EditText) findViewById(R.id.clientEmailIdET);
		feedbackTaxiNumber = (EditText) findViewById(R.id.feedbackTaxiNoET);
		feedbackCommentText = (EditText) findViewById(R.id.feedbackCommentET);

		feedbackStarRadioGroup = (RadioGroup) findViewById(R.id.feedbackRadioGroup);
		feedbackSubmitButton = (Button) findViewById(R.id.feedbackSubmitBt);
		namePattern = "[a-zA-Z ]+";
		emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

		clientName.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				name = clientName.getText().toString();
				if (name.matches(namePattern) && s.length() > 0) {
					feedbackSubmitButton.setEnabled(true);

				} else {
					clientName.setError("Invalid Name");
					feedbackSubmitButton.setEnabled(false);
					// Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
			}
		});

		clientEmail.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				email = clientEmail.getText().toString();
				if (email.matches(emailPattern) && s.length() > 0) {
					feedbackSubmitButton.setEnabled(true);

				} else {
					clientEmail.setError("Invalid email ID");
					feedbackSubmitButton.setEnabled(false);
					// Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
			}
		});

		feedbackSubmitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				name = clientName.getText().toString();
				email = clientEmail.getText().toString();
				regno = feedbackTaxiNumber.getText().toString();
				feedback = feedbackCommentText.getText().toString();
				selectedId = feedbackStarRadioGroup.getCheckedRadioButtonId();

				tempRadioButton = (RadioButton) findViewById(selectedId);
				rating = tempRadioButton.getText().toString();
				successMessage = (TextView) findViewById(R.id.successMessageTV);
				flagSuccess = false;

				if (name != null && email != null && regno != null
						&& feedback != null) {
					Thread thread = new Thread() {
						public void run() {
							try {
								HttpPost httppost = new HttpPost(ServerURLSt
										+ "feedbacksubmit/");
								List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
										5);
								nameValuePairs.add(new BasicNameValuePair(
										"name", name));
								nameValuePairs.add(new BasicNameValuePair(
										"emailid", email));
								nameValuePairs.add(new BasicNameValuePair(
										"regno", regno));
								nameValuePairs.add(new BasicNameValuePair(
										"feedback", feedback));
								nameValuePairs.add(new BasicNameValuePair(
										"rating", rating));

								httppost.setEntity(new UrlEncodedFormEntity(
										nameValuePairs));
								// ResponseHandler<String> responseHandler=new
								// BasicResponseHandler();

								HttpClient httpclient = new DefaultHttpClient();
								HttpResponse responseBody = httpclient
										.execute(httppost);
								String response = EntityUtils.toString(responseBody
										.getEntity());

								Log.i("Result", response);

								JSONObject JObject = new JSONObject(response);
								Log.i("Server code",""+responseBody.getStatusLine()
										.getStatusCode());
								
								if (responseBody.getStatusLine()
										.getStatusCode() == HttpStatus.SC_OK) {
									Log.i("response",
											"The server has responded successfully to feedback update");
/*									String responseSt = EntityUtils
											.toString(responseBody.getEntity());*/
									Log.i("Result", response);
									ReceivedResponse = JObject.getString("status");
									Log.i("Received Response", ReceivedResponse);
									if(ReceivedResponse.matches("Ok"))
									{
									flagSuccess = true;
									}
									else{
										flagSuccess=false;
									}
									// successMessage.setText("Your feedback has been successfully sent");

									// JSONObject JObject = new
									// JSONObject(response);
								} else {
/*									String responseSt = EntityUtils
											.toString(responseBody.getEntity());*/
									Log.i("Response status", ""
											+ responseBody.getStatusLine()
													.getStatusCode() + response);
									flagSuccess=false;
								}
							} catch (ClientProtocolException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					};
					thread.start();
					try {
						thread.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (flagSuccess)
						{successMessage
								.setText("Your feedback has been successfully sent");
						}
					else{
						successMessage
						.setText("Your feedback was not successfully updated.");
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Please enter all the details", Toast.LENGTH_SHORT)
							.show();

				}

			}
		});
	}

}
