package com.example.scratchtwo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.telephony.SmsMessage;


import com.example.scratchtwo.ReceiveMessage;

public class DriverHome extends Activity implements LocationListener {

	Intent myIntent;
	String ServerURLSt;

	ToggleButton DriverStatusTB;
	TextView ExtraTV;
	boolean isOn;
	LocationManager locationManager;
	LocationListener locationListener;
	boolean isGPSEnabled;
	String myTaxiNo;
	String latitude;
	String longitude;
	String msgStr;
	//BroadcastReceiver BR;
	//ReceiveMessage RM;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// TODO Create toggle button for changing status
		super.onCreate(savedInstanceState);
		setContentView(R.layout.driver_home);
		myIntent= getIntent();
		ServerURLSt = myIntent.getStringExtra("Server URL");
		
		latitude = "";
		longitude = "";
		isOn = true;
		DriverStatusTB = (ToggleButton) findViewById(R.id.driverStatusTB);
		ExtraTV = (TextView) findViewById(R.id.extraTV);
		myTaxiNo = myIntent.getStringExtra("taxi_no");

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				1000 * 10, 50, this);// Time b/w updates in ms and dist for
										// change in m
		// use phone number while updating GPS
		isGPSEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		DriverStatusTB.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isOn = ((ToggleButton) v).isChecked();

				if (isOn) {
					ExtraTV.setText("The driver status is now set to available.");
				} else {
					ExtraTV.setText("The driver status is now set to busy");
				}
				Thread sendChangeTr = new Thread() {
					public void run() {
						try {

							HttpPost httppost = new HttpPost(ServerURLSt
									+ "changetaxistatus/");// ask where to
																	// send
							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
									2);
							nameValuePairs.add(new BasicNameValuePair(
									"taxi_number", myTaxiNo)); // recheck send
																// name with him
							nameValuePairs.add(new BasicNameValuePair("status",
									isOn ? "available" : "busy")); // recheck
																	// name and
																	// also how
																	// to send
																	// status as
							httppost.setEntity(new UrlEncodedFormEntity(
									nameValuePairs));

							HttpClient httpclient = new DefaultHttpClient();
							HttpResponse responseBody = httpclient
									.execute(httppost);
							String response = EntityUtils.toString(responseBody
									.getEntity());
							Log.i("DH response Status code", ""
									+ responseBody.getStatusLine()
											.getStatusCode());
							Log.i("DH response code", response);
							if (responseBody.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
								Log.i("response",
										"The server has responded successfully");
								JSONObject Jobject = new JSONObject(response);
								/*
								 * JSONObject Jobject2 = new JSONObject(Jobject
								 * .getString("response"));
								 */
								Log.i("response", response);
								if (Jobject.getString("status").equals("Ok")) {

									Log.i("DH response",
											"Server replied Change accepted");
								}
							} else {
								Log.i("DH response",
										"Server replied but Change declined");
							}

						} catch (ClientProtocolException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				};
				sendChangeTr.start();
			}
		});// end of onToggleCLicked
		
		
/*		BR= new BroadcastReceiver(){

			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				 //---get the SMS message passed in---
				Log.i("DH entered","onReveive called");
		        Bundle bundle = intent.getExtras();        
		        SmsMessage[] msgs = null;
		        msgStr = "";            
		        if (bundle != null)
		        {
		            //---retrieve the SMS message received---
		            Object[] pdus = (Object[]) bundle.get("pdus");
		            msgs = new SmsMessage[pdus.length];            
		            for (int i=0; i<msgs.length; i++){
		                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);                
		                msgStr += "SMS from " + msgs[i].getOriginatingAddress();                     
		                msgStr += " :";
		                msgStr += msgs[i].getMessageBody().toString();
		                msgStr += "\n";        
		            }
		            //---display the new SMS message---
		            Toast.makeText(context, msgStr, Toast.LENGTH_SHORT).show();
		            ExtraTV.setText("str");
		        }
				
			}
			
		};*/
		
	/*	IntentFilter intentFilter = new IntentFilter("SmsMessage.intent.MAIN");
		Log.i("DH ","intent filter set");
		this.registerReceiver(RM, intentFilter);
		Log.i("DH ","receiver registered");*/
		
	}// end of onCreate

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		latitude = "" + location.getLatitude();
		longitude = "" + location.getLongitude();
		Thread thrd = new Thread() {
			public void run() {
				try {

					HttpPost httppost = new HttpPost(ServerURLSt
							+ "locationupdate/");

					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							2);
					nameValuePairs.add(new BasicNameValuePair("taxi_number",
							myTaxiNo));
					nameValuePairs.add(new BasicNameValuePair("location",
							latitude + "," + longitude));

					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					HttpClient httpclient = new DefaultHttpClient();
					HttpResponse responseBody = httpclient.execute(httppost);
					String response = EntityUtils.toString(responseBody
							.getEntity());
					if (responseBody.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						Log.i("response",
								"The server has responded successfully to driver login");
						Log.i("GPS update Status code ", ""
								+ responseBody.getStatusLine().getStatusCode());
						JSONObject Jobject = new JSONObject(response);
						Log.i("response", response);
						if (Jobject.getString("status").equals("Ok")) {
							Log.i("GPS update", "GPS updated successfully");
						} else {
							Log.i("GPS update",
									"Server reached but GPS not updated");
						}
					} else {
						Log.i("response Status", ""
								+ responseBody.getStatusLine().getStatusCode());

					}

				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		};
		thrd.start();
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.d("Latitude", "disable");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d("Latitude", "enable");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.d("Latitude", "status");
	}

}
