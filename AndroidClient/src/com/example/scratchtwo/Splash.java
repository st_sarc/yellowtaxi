package com.example.scratchtwo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Splash extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashxml);
		Thread timer = new Thread() {
			public void run() {
				try {
					sleep(1500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {

					Intent openTempActivity = new Intent(Splash.this,
							Temp.class);
					startActivity(openTempActivity);
					finish();
//
				}
			}
		};
		timer.start();

	}
}