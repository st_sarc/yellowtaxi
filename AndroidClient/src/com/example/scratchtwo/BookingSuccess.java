package com.example.scratchtwo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class BookingSuccess extends Activity {
	String journeyIdSt;
	String journeyLinkSt;
	String DriverNameSt;
	String DriverPhoneNoSt;
	String CarpoolStatusSt;
	String CustomerPhoneNoSt;
	String FromAddressSt;
	String ToAddressSt;
	ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.booking_success_list);
		Intent intent = getIntent();

		ArrayList<String> stringList = new ArrayList<String>();

		journeyIdSt = intent.getStringExtra("journey_id");
		journeyLinkSt = intent.getStringExtra("journey_link");
		DriverNameSt = intent.getStringExtra("driver_name");
		DriverPhoneNoSt = intent.getStringExtra("driver_phone_num");
		CarpoolStatusSt = intent.getStringExtra("carpool_status");
		FromAddressSt = intent.getStringExtra("from_address");
		ToAddressSt=intent.getStringExtra("to_address");
		CustomerPhoneNoSt = intent.getStringExtra("customer_phoneNo");

		stringList.add("Journey Id is: " + journeyIdSt);
		stringList.add("Journey link is: " + getString(R.string.server_url)
				+ journeyLinkSt);
		// ask about
		stringList.add("Driver Name is: " + DriverNameSt);
		stringList.add("Driver Phone Number is: " + DriverPhoneNoSt);
		stringList.add("Carpool Status: " + CarpoolStatusSt);

		listView = (ListView) findViewById(R.id.success_list);

		String[] strings = new String[stringList.size()];
		stringList.toArray(strings);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, strings);
		listView.setAdapter(adapter);

		String sms = "<customer phone number>" + CustomerPhoneNoSt
				+ "<Carpool status>" + CarpoolStatusSt + "<Pickup point>"
				+ FromAddressSt+"<Drop point>"+ToAddressSt;
		try {
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(DriverPhoneNoSt, null, sms, null, null);
			//smsManager.sendTextMessage("5554", null, sms, null, null);
			Toast.makeText(getApplicationContext(), "SMS Sent!",
					Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(),
					"SMS faild, please try again later!", Toast.LENGTH_LONG)
					.show();
			e.printStackTrace();
		}
	}

}
