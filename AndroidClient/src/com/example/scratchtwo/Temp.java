package com.example.scratchtwo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Temp extends Activity{
	EditText ServerURLET;
	String ServerURLSt;
	Button SetVal;
	String ServerURLPatternSt;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.temp);
		ServerURLPatternSt="http:\\/\\/.+\\/$";//http://anything/
		ServerURLET= (EditText) findViewById(R.id.serverURLET);
		SetVal= (Button) findViewById(R.id.setValBt);
		ServerURLET.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				ServerURLSt = ServerURLET.getText().toString();
				if (ServerURLSt.matches(ServerURLPatternSt) && s.length() > 0) {
					SetVal.setEnabled(true);

				} else {
					// email.setError("sdf", (drawable)R.drawable.ic_launcher);
					ServerURLET.setError("Invalid name");
					// Toast.makeText(getApplicationContext(),"Invalid name",Toast.LENGTH_SHORT).show();
					SetVal.setEnabled(false);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

		});
		SetVal.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				//R.string.server_url=ServerURL.getText().toString();
				Intent openClientActivity = new Intent(Temp.this,
						ClientLogin.class);
				ServerURLSt=ServerURLET.getText().toString();
				openClientActivity.putExtra("Server URL", ServerURLSt);
				startActivity(openClientActivity);
				finish();
/*				Intent openDriverLogin = new Intent(Temp.this,
						DriverLogin.class);
				ServerURLSt=ServerURLET.getText().toString();
				openDriverLogin.putExtra("Server URL", ServerURLSt);
				startActivity(openDriverLogin);
				finish();*/
			}
		});
		
		
	}
	

}
