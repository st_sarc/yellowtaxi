package com.example.scratchtwo;

import com.example.scratchtwo.TaxiList.Value;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TestMyAdapter extends ArrayAdapter<Value> {
	Context context;
	Value[] v;
	int layoutResourceId;

	public TestMyAdapter(Context context, int resource, Value[] v) {
		super(context, resource, v); // check if third parameter is necessary
		// TODO Auto-generated constructor stub
		this.v = v;
		this.context = context;
		this.layoutResourceId = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ValueHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new ValueHolder();
			holder.VendorName = (TextView) row.findViewById(R.id.vendorNameTV);
			holder.TaxiNumber = (TextView) row.findViewById(R.id.taxiNumberTV);
			holder.DriverName = (TextView) row.findViewById(R.id.driverNameTV);
			holder.DriverPhoneNo = (TextView) row
					.findViewById(R.id.driverPhoneNoTV);
			holder.Rating = (TextView) row.findViewById(R.id.ratingTV);

			row.setTag(holder);
		} else {
			holder = (ValueHolder) row.getTag();
		}

		Value val = v[position];
		
		if (val.VendorNameSt.matches(" ")) {
			holder.VendorName.setText(val.VendorNameSt);
			holder.TaxiNumber.setText(val.TaxiNumberSt);
			holder.DriverName.setText(val.DriverNameSt);
			holder.DriverPhoneNo.setText(val.DriverPhoneNoSt);
			holder.Rating.setText(val.RatingSt);
		} else {
			holder.VendorName.setText("Vendor Name: " + val.VendorNameSt);
			holder.TaxiNumber.setText("Taxi Number: " + val.TaxiNumberSt);
			holder.DriverName.setText("Driver Name: " + val.DriverNameSt);
			holder.DriverPhoneNo.setText("Drive Phone No. "
					+ val.DriverPhoneNoSt);
			holder.Rating.setText("Rating " + val.RatingSt);
		}
		return row;
	}

	static class ValueHolder {
		TextView VendorName;
		TextView TaxiNumber;
		TextView DriverName;
		TextView DriverPhoneNo;
		TextView Rating;
	}

}