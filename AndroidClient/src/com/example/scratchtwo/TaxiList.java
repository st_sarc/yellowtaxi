package com.example.scratchtwo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class TaxiList extends Activity {

	static class Value {
		String VendorNameSt, TaxiNumberSt, DriverNameSt, DriverPhoneNoSt,
				RatingSt;

		Value() {
			this.VendorNameSt = "khaali";
			this.TaxiNumberSt = "khaali";
			this.DriverNameSt = "khaali";
			this.DriverPhoneNoSt = "khaali";
			this.RatingSt = "khaali";
		}

		Value(String VendorNameSt, String TaxiNumberSt, String DriverNameSt,
				String DriverPhoneNoSt, String RatingSt) {
			this.VendorNameSt = VendorNameSt;
			this.TaxiNumberSt = TaxiNumberSt;
			this.DriverNameSt = DriverNameSt;
			this.DriverPhoneNoSt = DriverPhoneNoSt;
			this.RatingSt = RatingSt;
		}
	};

	Intent myIntent;
	String ServerURLSt;
	String PhoneNoSt;
	String PasswordSt;
	String FromAddressSt;
	String ToAddressSt;
	String NumberOfPassengersSt;
	String CarpoolStatusSt;
	String ReceivedResponse;
	ListView listView;
	Intent openSuccessPage;
	boolean isTaxiAvailable;
	boolean doPause;
	Long time;

	ArrayList<Value> valsList = new ArrayList<Value>();
	// Value vals[];
	TestMyAdapter adapter;

	TextView ListMessageTV;

	/*
	 * public void display(boolean isTaxiAvailable) {
	 * 
	 * }
	 */

	public void onCreate(Bundle someBundle) {
		super.onCreate(someBundle);
		setContentView(R.layout.taxi_listview);
		myIntent = getIntent();
		ServerURLSt = myIntent.getStringExtra("Server URL");
		ListMessageTV = (TextView) findViewById(R.id.listMessageTV);
		ListMessageTV.setText("Searching for a taxi");

		Log.i("TL setContent", "layout...notification");
		listView = (ListView) findViewById(R.id.list);

		Value v = new Value(" ", " ", " ", " ", " ");
		valsList.clear();
		valsList.add(v);

		Value[] vals = new Value[1];
		valsList.toArray(vals); // Because my adapter
								// only takes arrays and
								// not
								// list
		adapter = new TestMyAdapter(TaxiList.this, R.layout.taxi_items, vals);
		listView.setAdapter(adapter);

		Intent intent = getIntent();

		PhoneNoSt = intent.getStringExtra("phone_no");
		PasswordSt = intent.getStringExtra("password");
		FromAddressSt = intent.getStringExtra("from_address");
		ToAddressSt = intent.getStringExtra("to_address");
		NumberOfPassengersSt = intent.getStringExtra("passengers_num");
		CarpoolStatusSt = intent.getStringExtra("carpool_status");
		isTaxiAvailable = false;
		doPause = false;

		Log.i("Great", "The TaxiList activity has been started");
		Thread GetTaxiTr = new Thread() {
			// Thread to get all available taxis
			public void run() {
				while (isTaxiAvailable == false && doPause == false) {
					try {
						Log.i("TL taxiAvail", "At beginning of while"
								+ isTaxiAvailable);
						HttpPost httppost = new HttpPost(ServerURLSt
								+ "searchataxi/");
						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
								6);
						nameValuePairs.add(new BasicNameValuePair("phone_no",
								PhoneNoSt));
						nameValuePairs.add(new BasicNameValuePair("password",
								PasswordSt));
						nameValuePairs.add(new BasicNameValuePair(
								"from_address", FromAddressSt));
						nameValuePairs.add(new BasicNameValuePair("to_address",
								ToAddressSt));
						nameValuePairs.add(new BasicNameValuePair(
								"passengers_num", NumberOfPassengersSt));
						nameValuePairs.add(new BasicNameValuePair(
								"carpool_status", CarpoolStatusSt));

						httppost.setEntity(new UrlEncodedFormEntity(
								nameValuePairs));

						HttpClient httpclient = new DefaultHttpClient();
						HttpResponse responseBody = httpclient
								.execute(httppost);
						Log.i("Reponse code", ""
								+ responseBody.getStatusLine().getStatusCode());
						if (responseBody.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
							Log.i("response",
									"The server has responded successfully to taxi search");
							String response = EntityUtils.toString(responseBody
									.getEntity());

							Log.i("Result", response);

							JSONObject JObject = new JSONObject(response);
							ReceivedResponse = JObject.getString("status");
							Log.i("received status", ReceivedResponse);
							JSONArray JArray = JObject.getJSONArray("response");
							if (!(JArray.getString(0).matches("No taxis found"))) {
								JSONObject JObject2;

								valsList.clear();
								for (int i = 0; i < JArray.length(); i++) {
									JObject2 = JArray.getJSONObject(i);
									String VendorName = JObject2
											.getString("vendor_name");
									String DriverName = JObject2
											.getString("driver_name");
									String TaxiNumber = JObject2
											.getString("taxi_number");
									String DriverPhoneNo = JObject2
											.getString("phone_number");
									String Rating = JObject2
											.getString("rating");

									Log.i("Line" + i, VendorName + DriverName
											+ TaxiNumber);

									Value v = new Value(VendorName, TaxiNumber,
											DriverName, DriverPhoneNo, Rating);

									valsList.add(v);
								}
								Log.i("TL check",
										"Out of the for loop diplaying each result");
								isTaxiAvailable = true;
								Log.i("TL taxiAvail",
										"inside successful taxi search "
												+ isTaxiAvailable);
								/*
								 * runOnUiThread(new Runnable(){
								 * 
								 * public void run() {
								 * setContentView(R.layout.taxi_listview);
								 * 
								 * }});
								 */

								Value[] vals = new Value[valsList.size()];
								valsList.toArray(vals); // Because my adapter
														// only takes arrays and
														// not
														// list
								// adapter = new
								// TestMyAdapter(TaxiList.this,R.layout.taxi_items,
								// vals)
								Log.i("vals", vals[0].DriverNameSt
										+ vals[1].DriverNameSt);
								adapter = new TestMyAdapter(TaxiList.this,
										R.layout.taxi_items, vals);
								runOnUiThread(new Runnable() {

									public void run() {
										listView.setAdapter(adapter);
										adapter.notifyDataSetChanged();
										ListMessageTV
												.setText("Search successful");
									}
								});

								// adapter.notifyDataSetChanged();

								// listView.setAdapter(adapter);

							} else {
								isTaxiAvailable = false;
								time = (long) 7000;
								runOnUiThread(new Runnable() {

									public void run() { // no taxi found
										ListMessageTV
												.setText("No taxi found. Searching again in "
														+ time
														/ 1000
														+ " seconds.");
									}
								});

								sleep(time);
							}
						} else {
							Log.i("response",
									"The server response status is not 200 OK");
							isTaxiAvailable = false;
							time = (long) 7000;

							runOnUiThread(new Runnable() { // server error
								public void run() {
									ListMessageTV
											.setText("Server error. No taxi found. Searching again in "
													+ time / 1000 + " seconds.");
									;
								}
							});
							sleep(time);
						}
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}

					Log.i("TL taxiAval ", "outside loop" + isTaxiAvailable);
				} // end of while loop
			} // end of run function
		}; // end of thread definition
		GetTaxiTr.start();

		/*
		 * if(isTaxiAvailable == true) {
		 * Log.i("MainThread","diplaying taxi list");
		 * setContentView(R.layout.taxi_listview);
		 * 
		 * Value[] vals = new Value[valsList.size()]; valsList.toArray(vals); //
		 * Because my adapter // only takes arrays and // not // list
		 * TestMyAdapter adapter = new TestMyAdapter( TaxiList.this,
		 * R.layout.taxi_items, vals); listView.setAdapter(adapter); }
		 */

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				final Value itemValue = (Value) listView
						.getItemAtPosition(position);

				Log.i("Item clicked", itemValue.DriverNameSt);
				Thread trd = new Thread() { // Thread to book a taxi
					public void run() {
						try {

							HttpPost httppost2 = new HttpPost(ServerURLSt
									+ "booksubmit/");
							// Why am I sending the below values???
							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
							nameValuePairs.add(new BasicNameValuePair(
									"phone_no", PhoneNoSt));
							nameValuePairs.add(new BasicNameValuePair(
									"passengers_num", NumberOfPassengersSt));
							nameValuePairs.add(new BasicNameValuePair(
									"carpool_status", CarpoolStatusSt));
							nameValuePairs.add(new BasicNameValuePair(
									"taxi_number", itemValue.TaxiNumberSt));
							nameValuePairs.add(new BasicNameValuePair(
									"from_address", FromAddressSt));
							nameValuePairs.add(new BasicNameValuePair(
									"to_address", ToAddressSt));

							httppost2.setEntity(new UrlEncodedFormEntity(
									nameValuePairs));

							HttpClient httpclient2 = new DefaultHttpClient();
							Log.i("prebooktaxi", "Just before execution");
							HttpResponse responseBody2 = httpclient2
									.execute(httppost2);
							String response2 = EntityUtils
									.toString(responseBody2.getEntity());
							Log.i("Reponse code", ""
									+ responseBody2.getStatusLine()
											.getStatusCode());
							Log.i("response on taxi click", response2);
							if (responseBody2.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

								JSONObject JObject2 = new JSONObject(response2);
								if (JObject2.getString("status").matches("Ok")) {
									JSONObject JObject3 = JObject2
											.getJSONObject("response");
									String journeyIdSt = JObject3
											.getString("journey_id");
									String journeyLinkSt = JObject3
											.getString("journey_link");

									openSuccessPage = new Intent(TaxiList.this,
											BookingSuccess.class);
									openSuccessPage.putExtra("journey_id",
											journeyIdSt);
									openSuccessPage.putExtra("journey_link",
											journeyLinkSt);
									openSuccessPage.putExtra("driver_name",
											itemValue.DriverNameSt);
									openSuccessPage.putExtra(
											"driver_phone_num",
											itemValue.DriverPhoneNoSt);
									openSuccessPage.putExtra("carpool_status",
											CarpoolStatusSt);
									openSuccessPage.putExtra("from_address",
											FromAddressSt);
									openSuccessPage.putExtra("to_address",
											ToAddressSt);
									openSuccessPage.putExtra(
											"customer_phoneNo", PhoneNoSt);

									if (openSuccessPage.getExtras() != null) {
										startActivity(openSuccessPage);
										finish();
									}

								} else {
									Log.i("Response to taxi click",
											"Message received but Not Ok");
								}
							} else {
								Log.i("Response to taxi click",
										"bad response code");
							}

						} catch (ClientProtocolException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				};
				trd.start(); // Find a better method to see if intent is set

				// if (openSuccessPage.getExtras()!=null) {
				// startActivity(openSuccessPage); finish(); }

			}// end of OnItemClick
		}); // end of listview onCLickListener

	} // end of onCreate

	public void onPause() {
		super.onPause();
		doPause = true;
	}
} // end of TaxiList extends Activity