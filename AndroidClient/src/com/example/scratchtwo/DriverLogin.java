package com.example.scratchtwo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DriverLogin extends Activity {
	String ServerURLSt;
	EditText TaxiNo;
	EditText password;
	Button driverBt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.driver_login);
		Intent myIntent=getIntent();
		ServerURLSt=myIntent.getStringExtra("Server URL");
		TaxiNo = (EditText) findViewById(R.id.taxiNoET);
		password = (EditText) findViewById(R.id.driverPasswordET);
		driverBt= (Button) findViewById(R.id.driverLoginBt);


		driverBt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Thread thred = new Thread() {
					public void run() {
						try {
							Log.i("Thread", "Thread started in driver login");

							HttpPost httppost = new HttpPost(
									ServerURLSt + "driverlogin/");

							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
									2);
							nameValuePairs.add(new BasicNameValuePair("taxi_number",
									TaxiNo.getText().toString()));
							nameValuePairs.add(new BasicNameValuePair("password",
									password.getText().toString()));

							httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

							HttpClient httpclient = new DefaultHttpClient();
							HttpResponse responseBody = httpclient.execute(httppost);
							String response = EntityUtils.toString(responseBody
									.getEntity());
							if (responseBody.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
								Log.i("response",
										"The server has responded successfully to driver login");
								Log.i("Driver login Status code ", ""
										+ responseBody.getStatusLine().getStatusCode());
								JSONObject Jobject = new JSONObject(response);
								/*
								 * JSONObject Jobject2 = new JSONObject(Jobject
								 * .getString("response"));
								 */
								Log.i("response", response);
								if (Jobject.getString("status").equals("Ok")) {

									// Now open
									
									Intent openDriverHome = new Intent(
											DriverLogin.this, DriverHome.class);
									openDriverHome.putExtra("taxi_no", TaxiNo
											.getText().toString());
									openDriverHome.putExtra("Server URL", ServerURLSt);
									startActivity(openDriverHome);
								}
							} else {
								Log.i("response Status", ""
										+ responseBody.getStatusLine().getStatusCode());

							}

						} catch (ClientProtocolException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				};
				thred.start();
				
				
			}
		});

	}

}
