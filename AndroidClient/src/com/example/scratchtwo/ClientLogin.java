package com.example.scratchtwo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class ClientLogin extends Activity implements LocationListener {
	String ServerURLSt;
	EditText PhoneNo;
	EditText Password;
	EditText FromAddress;
	EditText ToAddress;
	EditText NumberOfPassengers;
	int RBId;
	RadioGroup CarpoolRG;
	RadioButton tempRB;

	String PhoneNoPattern;
	String NOPPattern;

	Button GetCurrentAddress;
	Button LoginAndSearch;
	Button signUpClient;
	Button loginDriver;
	Button feedback;

	String PhoneNoSt;
	String PasswordSt;
	String FromAddressSt;
	String ToAddressSt;
	String NumberOfPassengersSt;
	String latitude;
	String longitude;
	String carpoolStatus;

	LocationManager locationManager;
	LocationListener locationListener;
	Location curLocation;
	boolean isGPSEnabled;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.client_login);
		Intent myIntent = getIntent();
		ServerURLSt = myIntent.getStringExtra("Server URL");
		PhoneNo = (EditText) findViewById(R.id.clientPhoneNoET);
		Password = (EditText) findViewById(R.id.clientPasswordET);
		FromAddress = (EditText) findViewById(R.id.clientFromAddressET);
		ToAddress = (EditText) findViewById(R.id.clientToAddressET);
		NumberOfPassengers = (EditText) findViewById(R.id.numberOfPassengers);

		GetCurrentAddress = (Button) findViewById(R.id.clientGetCurrentAddressBt);
		LoginAndSearch = (Button) findViewById(R.id.clientLoginAndSearchBt);
		CarpoolRG = (RadioGroup) findViewById(R.id.carpoolRG);
		RBId = CarpoolRG.getCheckedRadioButtonId();

		tempRB = (RadioButton) findViewById(RBId);
		carpoolStatus = tempRB.getText().toString();
		signUpClient = (Button) findViewById(R.id.signUpBt);
		loginDriver = (Button) findViewById(R.id.loginDriverBt);
		feedback = (Button) findViewById(R.id.feedbackBt);
		PhoneNoPattern = "((\\+[0-9]{2,3} ?)|0)?[0-9]{10}"; // check if | is a valid thing to do
		NOPPattern = "[1-9][0-9]?";

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				1000, 50, this);// Time b/w updates in ms and dist for change in
		isGPSEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (isGPSEnabled) {
			curLocation = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			try {
				latitude = "" + curLocation.getLatitude();
				longitude = "" + curLocation.getLongitude();
			} catch (NullPointerException e) {
				latitude = "12";
				longitude = "76";
			}
		} else {
			FromAddress.setText("Please turn on the GPS first");
		}
		//
		PhoneNo.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (PhoneNo.getText().toString().matches(PhoneNoPattern)
						&& s.length() > 0) {
					LoginAndSearch.setEnabled(true);

				} else {
					PhoneNo.setError("Invalid Phone");
					LoginAndSearch.setEnabled(false);
					// Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

		});
		NumberOfPassengers.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				if (NumberOfPassengers.getText().toString().matches(NOPPattern)
						&& s.length() > 0) {
					LoginAndSearch.setEnabled(true);

				} else {
					NumberOfPassengers.setError("Invalid number of passengers");
					LoginAndSearch.setEnabled(false);
					// Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

		});

		GetCurrentAddress.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				isGPSEnabled = locationManager
						.isProviderEnabled(LocationManager.GPS_PROVIDER);

				if (isGPSEnabled) {
					curLocation = locationManager
							.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					try{

					latitude = "" + curLocation.getLatitude();
					longitude = "" + curLocation.getLongitude();
					FromAddress.setText(latitude + "," + longitude);
					}
					catch(NullPointerException e){
						FromAddress.setText(latitude+","+longitude);
					}
					

				} else {
					FromAddress.setText("Please turn on the GPS first");
				}
			}

		});

		LoginAndSearch.setOnClickListener(new View.OnClickListener() {
			// perform network activities in mainActivity

			@Override
			public void onClick(View v) {
				PhoneNoSt = PhoneNo.getText().toString();
				PasswordSt = Password.getText().toString();
				FromAddressSt = FromAddress.getText().toString();
				ToAddressSt = ToAddress.getText().toString();
				NumberOfPassengersSt = NumberOfPassengers.getText().toString();

				Log.i("CL phone number is", "" + PhoneNoSt);
				Log.i("CL Password is", PasswordSt);
				Log.i("CL FromAddressis", FromAddressSt);
				Log.i("CL ToAddress is", ToAddressSt);
				Log.i("CL NumberOfPassengers is", NumberOfPassengersSt);

				if (!(PhoneNoSt.matches("") || PasswordSt.matches("")
						|| FromAddressSt.matches("") || ToAddressSt.matches("") || NumberOfPassengersSt
						.matches(""))) {

					Log.i("CL phone number is", " " + PhoneNoSt);
					Log.i("CL Password is", " " + PasswordSt);
					Log.i("CL FromAddressis", " " + FromAddressSt);
					Log.i("CL ToAddress is", " " + ToAddressSt);
					Log.i("CL NumberOfPassengers is", " "
							+ NumberOfPassengersSt);

					Log.i("CL", "All strings have been filled");

					Intent openTaxiList = new Intent(ClientLogin.this,
							TaxiList.class);
					Log.i("intent", "All intent set");
					openTaxiList.putExtra("phone_no", PhoneNoSt); // Optional
																	// parameters
					openTaxiList.putExtra("password", PasswordSt);
					openTaxiList.putExtra("from_address", FromAddressSt);
					openTaxiList.putExtra("to_address", ToAddressSt);
					openTaxiList.putExtra("carpool_status", carpoolStatus);
					openTaxiList.putExtra("passengers_num", NumberOfPassengers
							.getText().toString());
					openTaxiList.putExtra("Server URL", ServerURLSt);

					Log.i("Extra", "All extras put");
					startActivity(openTaxiList);
				} else {//hi
					Toast.makeText(getApplicationContext(),
							"Enter all the details", Toast.LENGTH_SHORT).show();
				}
			}

		});
		// set on clicks here
		signUpClient.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openClientSignupActivity = new Intent(ClientLogin.this,
						ClientSignup.class);
				openClientSignupActivity.putExtra("Server URL", ServerURLSt);
				startActivity(openClientSignupActivity);
				// finish();
			}
		});
		loginDriver.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent openDriverLogin = new Intent(ClientLogin.this,
						DriverLogin.class);
				openDriverLogin.putExtra("Server URL", ServerURLSt);
				startActivity(openDriverLogin);
				// finish();

			}
		});
		feedback.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openFeedback = new Intent(ClientLogin.this,
						Feedback.class);
				openFeedback.putExtra("Server URL", ServerURLSt);
				startActivity(openFeedback);
				// finish();
			}
		});
	}

	@Override
	public void onLocationChanged(Location location) {
		// Will this be called if GPS is disabled?
		isGPSEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		if (isGPSEnabled) {
			latitude = "" + location.getLatitude();
			longitude = "" + location.getLongitude();
			FromAddress.setText(latitude + "," + longitude);
		} else {
			FromAddress.setText("Please turn on the GPS first");
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.i("Location", "Provider disabled");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.i("Location", "Provider Enabled");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.i("Location", "Status Changed");
	}

}
