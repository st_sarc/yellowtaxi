package com.example.scratchtwo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.example.scratchtwo.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ClientSignup extends Activity {

	EditText name;
	EditText pass;
	EditText email;
	EditText phoneNo;
	EditText address;
	Button signUp;

	String ServerURLSt;
	String nameSt;
	String passSt;
	String emailSt;
	String phoneNoSt;
	String addressSt;

	String namePattern;
	String phoneNoPattern;
	String emailPattern;
	String passPattern;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.client_signup);
		// setContentView();
		Intent myIntent = getIntent();
		ServerURLSt = myIntent.getStringExtra("Server URL");

		name = (EditText) findViewById(R.id.nameET);
		pass = (EditText) findViewById(R.id.passwordET);
		email = (EditText) findViewById(R.id.emailET);
		phoneNo = (EditText) findViewById(R.id.phoneNoET);
		address = (EditText) findViewById(R.id.addressET);
		signUp = (Button) findViewById(R.id.signUpBT);

		namePattern = "[a-zA-Z ]+";
		phoneNoPattern = "((\\+[0-9]{2,3} ?)|0)?[0-9]{10}";;
		
		emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";// .... +\.[a-z]+

		name.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				nameSt = name.getText().toString();
				if (nameSt.matches(namePattern) && s.length() > 0) {
					signUp.setEnabled(true);

				} else {
					// email.setError("sdf", (drawable)R.drawable.ic_launcher);
					name.setError("Invalid name");
					// Toast.makeText(getApplicationContext(),"Invalid name",Toast.LENGTH_SHORT).show();
					signUp.setEnabled(false);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

		});
		email.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				emailSt = email.getText().toString();
				if (emailSt.matches(emailPattern) && s.length() > 0) {
					signUp.setEnabled(true);

				} else {
					email.setError("Invalid email ID");
					signUp.setEnabled(false);
					// Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

		});
		phoneNo.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				phoneNoSt = phoneNo.getText().toString();
				if (phoneNoSt.matches(phoneNoPattern) && s.length() > 0) {
					signUp.setEnabled(true);

				} else {
					phoneNo.setError("Invalid Phone address");
					// Toast.makeText(getApplicationContext(),"Invalid phone address",Toast.LENGTH_SHORT).show();
					signUp.setEnabled(false);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

		});
		pass.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				passSt = pass.getText().toString();
				if (passSt.length()>6 && s.length() > 0) {
					signUp.setEnabled(true);

				} else {
					pass.setError("Enter more than 6 characters");
					// Toast.makeText(getApplicationContext(),"Invalid phone address",Toast.LENGTH_SHORT).show();
					signUp.setEnabled(false);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

		});
		signUp.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Log.i("press", "The button has been pressed");
				nameSt = name.getText().toString();
				passSt = pass.getText().toString();
				emailSt = email.getText().toString();
				phoneNoSt = phoneNo.getText().toString();
				addressSt = address.getText().toString();

				Log.i("userANDpass", nameSt + passSt);
				// A new thread is being created because it is illegal to
				// perform network activities in mainActivity
				if (nameSt != null && passSt != null && emailSt != null
						&& phoneNoSt != null && addressSt != null) {

					Thread tr = new Thread() {
						public void run() {
							try {

								HttpPost httppost = new HttpPost(ServerURLSt
										+ "clientsignup/");
								List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
										5);
								nameValuePairs.add(new BasicNameValuePair(
										"name", nameSt));
								nameValuePairs.add(new BasicNameValuePair(
										"password", passSt));
								nameValuePairs.add(new BasicNameValuePair(
										"email_id", emailSt));
								nameValuePairs.add(new BasicNameValuePair(
										"phone_no", phoneNoSt));
								nameValuePairs.add(new BasicNameValuePair(
										"address", addressSt));

								httppost.setEntity(new UrlEncodedFormEntity(
										nameValuePairs));

								HttpClient httpclient = new DefaultHttpClient();
								HttpResponse responseBody = httpclient
										.execute(httppost);
								String response = EntityUtils
										.toString(responseBody.getEntity());
								if (responseBody.getStatusLine()
										.getStatusCode() == HttpStatus.SC_OK) {
									Log.i("response",
											"The server has responded successfully");
									Log.i("Client sign up Status code ", ""
											+ responseBody.getStatusLine()
													.getStatusCode());
									JSONObject Jobject = new JSONObject(
											response);
									/*
									 * JSONObject Jobject2 = new
									 * JSONObject(Jobject
									 * .getString("response"));
									 */
									Log.i("response", response);
									if (Jobject.getString("status")
											.equals("Ok")) {

										// Now open
										Intent openClientLogin = new Intent(
												ClientSignup.this,
												ClientLogin.class);
										startActivity(openClientLogin);
									}
								} else {
									Log.i("response Status", ""
											+ responseBody.getStatusLine()
													.getStatusCode());
									Log.i("The response", response);
									Toast.makeText(ClientSignup.this,
											"Bad response code",
											Toast.LENGTH_SHORT).show();

								}

							} catch (ClientProtocolException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}

						}

					};
					tr.start();
				} else {
					Toast.makeText(getApplicationContext(),
							"Please enter all the details", Toast.LENGTH_SHORT)
							.show();

				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}